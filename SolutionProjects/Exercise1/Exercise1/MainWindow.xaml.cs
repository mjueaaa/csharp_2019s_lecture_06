﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Exercise1 {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {

        public class MyEventArgs { }
        

        // Status change
        public class MyStatusChangedEventArgs : MyEventArgs {
            public MyStatusChangedEventArgs(string new_status) { NewStatus = new_status; }
            public string NewStatus { get; set; }
        }
        public delegate void StatusChangedHandler(object sender, MyStatusChangedEventArgs e);
        public event StatusChangedHandler StatusChangedEvent;
        private void NotifyStatusChanged(string status) {
            if (StatusChangedEvent != null)
                StatusChangedEvent(this, new MyStatusChangedEventArgs(status));
        }

        // Send messages
        public class MySendEventArgs : MyEventArgs {
            public MySendEventArgs(string message) { Message = message; }
            public string Message { get; set; }
        }
        public delegate void SendMessageHandler(object sender, MySendEventArgs e);
        public event SendMessageHandler SendMessageEvent;
        private void NotifySendMessage(string message) {
            if (SendMessageEvent != null)
                SendMessageEvent(this, new MySendEventArgs(message));
        }

        // Timer event
        public class MyTimerEventArgs : MyEventArgs {
            public MyTimerEventArgs() { Time = DateTime.Now; }
            public DateTime Time { get; set; }
        }
        public delegate void TimerEventHandler(object sender, MyTimerEventArgs e);
        public event TimerEventHandler TimerEvent;
        private void NotifyTimerEvent() {
            if (TimerEvent != null)
                TimerEvent(this, new MyTimerEventArgs());
        }

        public MainWindow() {
            InitializeComponent();

            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(10);            
            timer.Tick += Timer_Tick;
            timer.Start();
        }

        

        private void Dummy_Click(object sender, RoutedEventArgs e) {
            EventWindow w = new EventWindow(this);
            w.Show();
        }

        private void RadioStopped_Checked(object sender, RoutedEventArgs e) {
            NotifyStatusChanged("Stopped");
        }

        private void RadioStandBy_Checked(object sender, RoutedEventArgs e) {
            NotifyStatusChanged("StandBy");
        }

        private void RadioActive_Checked(object sender, RoutedEventArgs e) {
            NotifyStatusChanged("Active");
        }

        private void SendButton_Click(object sender, RoutedEventArgs e) {
            NotifySendMessage(textBox.Text);
        }

        private void Timer_Tick(object sender, EventArgs e) {
            NotifyTimerEvent();
        }
    }
}
