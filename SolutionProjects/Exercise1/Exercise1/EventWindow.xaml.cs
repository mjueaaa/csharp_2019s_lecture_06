﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Exercise1 {
    /// <summary>
    /// Interaction logic for EventWindow.xaml
    /// </summary>
    public partial class EventWindow : Window {
        public EventWindow(MainWindow main) {
            InitializeComponent();
            main.StatusChangedEvent += StatusChangedHandler;
            main.SendMessageEvent += SendMessageHandler;
            main.TimerEvent += TimerEventHandler;
        }

        void StatusChangedHandler(object sender, MainWindow.MyStatusChangedEventArgs e) {
            labelStatus.Content = e.NewStatus;
        }
        void SendMessageHandler(object sender, MainWindow.MySendEventArgs e) {
            textBox.AppendText(e.Message + "\n");
        }
        void TimerEventHandler(object sender, MainWindow.MyTimerEventArgs e) {
            labelTime.Content = e.Time.ToString();
        }
    }
}
